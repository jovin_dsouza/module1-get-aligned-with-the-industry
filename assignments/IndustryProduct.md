**Product Name**

Automated Door Locking System

**Product Link**

[Link](https://www.identicard.com/access-control/premisys-with-wireless-locks/)

**Product Short Description**

This product is attached to the cricket bat and it helps in detecting the speed of the bat when swinged.

**Product is combination of features**

1. ID Recognition 
2. Object Detection
3. Segmentation


**Product is provided by which company?**

IdentiCard

-------------------------------------------------------------------------------------------------------------------------------------------------------
**Product Name**

Label Tagging

**Product Link**

[Link](https://www.clarifai.com/)

**Product Short Description**

Makes it easy for your customers to discover your products online. Provide them with faster-expanded search results to reduce bounce rates and make purchase decisions faster. 

**Product is combination of features**

1. Object detection
2. Segmentation


**Product is provided by which company?**

Clarifai